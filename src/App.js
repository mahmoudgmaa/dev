import React, { useState, useEffect } from "react";
import Carousel from "./components/carousel/index";
import data from "./models/dummy_data.json";
const App = () => {
  const [dummyData, setDummyData] = useState([]);
  useEffect(() => {
    setDummyData(data);
  }, []);
  return (
    <>
      <Carousel dummyData={dummyData} />
    </>
  );
};
export default App;
